package main

import (
	"log"
	"os"
	"testing"

	"github.com/pilu/miniassert"
)

func TestStore_take(t *testing.T) {
	f, err := os.Open("fixtures/mobydick.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	c := newWordFrequencyCalculator(f)
	nodes := c.take(20)

	expectedResults := []struct {
		frequency int
		word      string
	}{
		{4284, "the"},
		{2192, "and"},
		{2185, "of"},
		{1861, "a"},
		{1685, "to"},
		{1366, "in"},
		{1056, "i"},
		{1024, "that"},
		{889, "his"},
		{821, "it"},
		{783, "he"},
		{616, "but"},
		{603, "was"},
		{595, "with"},
		{577, "s"},
		{564, "is"},
		{551, "for"},
		{542, "all"},
		{541, "as"},
		{458, "at"},
	}

	for i, exp := range expectedResults {
		miniassert.Equal(t, exp.frequency, nodes[i].freq)
		miniassert.Equal(t, exp.word, nodes[i].wn.w.String())
	}
}
