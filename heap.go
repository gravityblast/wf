package main

type heapNode struct {
	wn   *wordNode
	freq int
}

type heap struct {
	size  int
	nodes []*heapNode
}

func newHeap(n int) *heap {
	h := &heap{
		nodes: make([]*heapNode, n),
	}

	for i := 0; i < n; i++ {
		h.nodes[i] = &heapNode{wn: &wordNode{w: newWord(), heapIndex: -1}}
	}

	return h
}

func (h *heap) swap(a, b *heapNode) {
	tmp := *a
	*a = *b
	*b = tmp
}

func (h *heap) minheapify(i int) {
	leftPos := 2*i + 1
	rightPos := 2*i + 2
	smallest := i

	if leftPos < h.size && h.nodes[leftPos].freq < h.nodes[i].freq {
		smallest = leftPos
	}

	if rightPos < h.size && h.nodes[rightPos].freq < h.nodes[smallest].freq {
		smallest = rightPos
	}

	if smallest != i {
		h.nodes[smallest].wn.heapIndex = i
		h.nodes[i].wn.heapIndex = smallest
		h.swap(h.nodes[smallest], h.nodes[i])
		h.minheapify(smallest)
	}
}

func (h *heap) build() {
	for i := h.size/2 - 1; i >= 0; i-- {
		h.minheapify(i)
	}
}

func (h *heap) buildSorted(acc []*heapNode) []*heapNode {
	if h.size == 0 {
		return acc
	}

	acc = append([]*heapNode{h.nodes[0]}, acc...)
	h.nodes[0] = h.nodes[h.size-1]
	h.size--
	h.minheapify(0)
	return h.buildSorted(acc)
}

func (h *heap) sort() []*heapNode {
	return h.buildSorted([]*heapNode{})
}

func (h *heap) siftDown(start int, end int) {
	root := start
	for {
		child := root*2 + 1

		if child >= end {
			break
		}

		if child < end && h.nodes[child].freq < h.nodes[child+1].freq {
			child++
		}

		if h.nodes[root].freq < h.nodes[child].freq {
			h.nodes[root], h.nodes[child] = h.nodes[child], h.nodes[root]
			root = child
		} else {
			break
		}
	}
}

func (h *heap) insert(wn *wordNode) {
	if wn.heapIndex > -1 {
		h.nodes[wn.heapIndex].freq = wn.freq
		h.minheapify(wn.heapIndex)
		return
	}

	if h.size < len(h.nodes) {
		count := h.size
		wn.heapIndex = h.size
		h.nodes[count] = &heapNode{
			wn:   wn,
			freq: wn.freq,
		}
		h.size++
		h.build()
		return
	}

	if wn.freq > h.nodes[0].freq {
		h.nodes[0].wn.heapIndex = -1
		h.nodes[0].wn = wn
		h.nodes[0].wn.heapIndex = 0
		h.nodes[0].freq = wn.freq

		h.minheapify(0)
	}
}
