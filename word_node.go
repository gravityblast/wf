package main

type wordNode struct {
	w         *word
	left      *wordNode
	right     *wordNode
	freq      int
	heapIndex int
}

func (n *wordNode) swap(other *wordNode) {
	*n, *other = *other, *n
}

func (n *wordNode) lessThan(n2 *wordNode) bool {
	return n.freq < n2.freq
}

func (n *wordNode) greaterThan(n2 *wordNode) bool {
	return n.freq > n2.freq
}

type tree struct {
	root *wordNode
}

func (t *tree) insert(w *word) *wordNode {
	n, x := t.insertNode(t.root, w)

	if t.root == nil {
		t.root = n
	}

	return x
}

func (t *tree) insertNode(root *wordNode, w *word) (*wordNode, *wordNode) {
	if root == nil {
		n := &wordNode{
			w:         w,
			heapIndex: -1,
		}

		return n, n
	}

	switch w.compare(root.w) {
	case COMPARISON_LESS:
		n, x := t.insertNode(root.left, w)
		root.left = n
		return root, x
	case COMPARISON_GREATER:
		n, x := t.insertNode(root.right, w)
		root.right = n
		return root, x
	}

	return root, root
}

// avg O(log n), worst case O(n)
func (t *tree) search(w *word) *wordNode {
	if t.root == nil {
		return nil
	}

	return t.searchNode(t.root, w)
}

func (t *tree) searchNode(root *wordNode, w *word) *wordNode {
	if root == nil {
		return nil
	}

	switch w.compare(root.w) {
	case COMPARISON_EQUAL:
		return root
	case COMPARISON_LESS:
		return t.searchNode(root.left, w)
	default:
		return t.searchNode(root.right, w)
	}
}
