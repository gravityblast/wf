package main

import (
	"testing"

	"github.com/pilu/miniassert"
	"github.com/stretchr/testify/assert"
)

func TestWord(t *testing.T) {
	w := newWord()
	miniassert.Nil(t, w.head)

	r := 'b'
	w.addRune(r)
	miniassert.Equal(t, 'b', *w.head)
	miniassert.Nil(t, w.tail)

	r = 'a'
	w2 := w.addRune(r)
	miniassert.Equal(t, w2, w.tail)
	miniassert.Equal(t, 'a', *w2.head)
	miniassert.Nil(t, w2.tail)

	r = 'r'
	w3 := w.addRune(r)
	miniassert.Equal(t, w3, w2.tail)
	miniassert.Equal(t, 'r', *w3.head)
	miniassert.Nil(t, w3.tail)

	w3.addRune('x', 'y', 'z')

	miniassert.Equal(t, "barxyz", w.String())
}

func TestWord_compare(t *testing.T) {
	w1 := newWord()
	w2 := newWord()
	assert.Equal(t, COMPARISON_EQUAL, w1.compare(w2))

	w1 = newWord()
	w2 = newWord('a')
	assert.Equal(t, COMPARISON_LESS, w1.compare(w2))

	w1 = newWord('a')
	w2 = newWord()
	assert.Equal(t, COMPARISON_GREATER, w1.compare(w2))

	w1 = newWord('f', 'o', 'o')
	w2 = newWord('f', 'o', 'o')
	assert.Equal(t, COMPARISON_EQUAL, w1.compare(w2))

	w1 = newWord('f', 'o', 'o')
	w2 = newWord('b', 'a', 'r')
	assert.Equal(t, COMPARISON_GREATER, w1.compare(w2))

	w1 = newWord('b', 'a', 'r')
	w2 = newWord('f', 'o', 'o')
	assert.Equal(t, COMPARISON_LESS, w1.compare(w2))

	w1 = newWord('f', 'o', 'o')
	w2 = newWord('f', 'o', 'o', 'd')
	assert.Equal(t, COMPARISON_LESS, w1.compare(w2))

	w1 = newWord('f', 'o', 'o', 'd')
	w2 = newWord('f', 'o', 'o')
	assert.Equal(t, COMPARISON_GREATER, w1.compare(w2))
}

func BenchmarkNewWord(b *testing.B) {
	for i := 0; i < b.N; i++ {
		newWord('a', 'b', 'c', 'd', 'e')
	}
}
