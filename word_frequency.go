package main

import "io"

type wordFrequencyCalculator struct {
	s *scanner
}

func newWordFrequencyCalculator(r io.Reader) *wordFrequencyCalculator {
	return &wordFrequencyCalculator{
		s: newScanner(r),
	}
}

func (c *wordFrequencyCalculator) take(n int) []*heapNode {
	t := &tree{}
	h := newHeap(n)

	for w, ok := c.s.scan(); ok; w, ok = c.s.scan() {
		node := t.insert(w)
		node.freq++
		h.insert(node)
	}

	return h.sort()
}
