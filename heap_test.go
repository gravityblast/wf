package main

import (
	"testing"

	"github.com/pilu/miniassert"
)

func newTestWordNode(h *heap, freq int, rs ...rune) *wordNode {
	wn := &wordNode{
		w:         newWord(rs...),
		heapIndex: -1,
		freq:      freq,
	}
	h.insert(wn)

	return wn
}

func TestHeap(t *testing.T) {
	h := newHeap(5)
	newTestWordNode(h, 5, 'a')
	b := newTestWordNode(h, 1, 'b', 'o', 'o')
	newTestWordNode(h, 1, 'c')
	newTestWordNode(h, 3, 'd')
	newTestWordNode(h, 4, 'e')
	newTestWordNode(h, 2, 'f')
	newTestWordNode(h, 9, 'g')

	h.sort()
	testHeap(t, h, 'g', 'a', 'e', 'd', 'f')

	b.freq = 10
	h.insert(b)

	testHeap(t, h, 'b', 'g', 'a', 'e', 'd')
}

func testHeap(t *testing.T, h *heap, rs ...rune) {
	h.sort()
	miniassert.Equal(t, len(rs), len(h.nodes))
}
