package main

import (
	"testing"

	"github.com/pilu/miniassert"
	"github.com/stretchr/testify/assert"
)

func TestTree(t *testing.T) {
	tree := &tree{}
	ab := newWord('a', 'b')
	tree.insert(ab)
	miniassert.Equal(t, ab, tree.root.w)

	abz := newWord('a', 'b', 'z')
	tree.insert(abz)
	miniassert.Equal(t, abz, tree.root.right.w)

	abc := newWord('a', 'b', 'c')
	tree.insert(abc)
	tree.insert(abc)
	tree.insert(abc)
	miniassert.Equal(t, abc, tree.root.right.left.w)

	res := tree.search(newWord('a', 'b'))
	assert.NotNil(t, res)
	assert.Equal(t, "ab", res.w.String())

	res = tree.search(newWord('a', 'b', 'z'))
	assert.NotNil(t, res)
	assert.Equal(t, "abz", res.w.String())

	res = tree.search(newWord('a', 'b', 'c'))
	assert.NotNil(t, res)
	assert.Equal(t, "abc", res.w.String())

	res = tree.search(newWord('x'))
	assert.Nil(t, res)
}
