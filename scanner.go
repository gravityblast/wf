package main

import (
	"bufio"
	"io"
	"unicode"
	"unicode/utf8"
)

type scanner struct {
	sc *bufio.Scanner
}

func newScanner(r io.Reader) *scanner {
	sc := bufio.NewScanner(r)
	sc.Split(bufio.ScanRunes)

	return &scanner{
		sc: sc,
	}
}

func (s *scanner) scan() (*word, bool) {
	w := newWord()

	for s.sc.Scan() {
		r, _ := utf8.DecodeRune(s.sc.Bytes())
		if !unicode.In(r, unicode.L) {
			if w.head == nil {
				continue
			}

			return w, true
		}

		r = unicode.ToLower(r)
		w.addRune(r)
	}

	if w.head != nil {
		return w, true
	}

	return nil, false
}
