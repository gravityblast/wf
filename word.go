package main

import "fmt"

const (
	COMPARISON_LESS = iota
	COMPARISON_EQUAL
	COMPARISON_GREATER
)

type word struct {
	head *rune
	tail *word
}

func newWord(rs ...rune) *word {
	w := &word{}
	for _, r := range rs {
		w.addRune(r)
	}

	return w
}

func (w *word) addRune(rs ...rune) *word {
	tmp := w
	for _, r := range rs {
		// use an anonymous func to avoid overriding the value of the same variable
		// causing r to always be the last value
		func(r rune) {
			tmp = w.add(&r)
		}(r)
	}

	return tmp
}

func (w *word) add(r *rune) *word {
	if w.head == nil {
		w.head = r
		return w
	}

	if w.tail == nil {
		w.tail = newWord()
		w.tail.add(r)
		return w.tail
	}

	tmp := w
	for tmp.tail != nil {
		tmp = tmp.tail
	}

	tmp.tail = newWord()
	tmp.tail.add(r)

	return tmp.tail
}

func (w *word) compare(w2 *word) int {
	if w.head == nil && w2.head == nil {
		return COMPARISON_EQUAL
	}

	if w2 == nil || w2.head == nil {
		return COMPARISON_GREATER
	}

	if w.head == nil {
		return COMPARISON_LESS
	}

	if *w.head > *w2.head {
		return COMPARISON_GREATER
	}

	if *w.head < *w2.head {
		return COMPARISON_LESS
	}

	if w.tail == nil && w2.tail != nil {
		return COMPARISON_LESS
	}

	if w.tail == nil && w2.tail == nil {
		return COMPARISON_EQUAL
	}

	return w.tail.compare(w2.tail)
}

func (w *word) String() string {
	if w.head == nil {
		return ""
	}

	s := string(*w.head)

	if w.tail == nil {
		return s
	}

	return fmt.Sprintf("%s%s", s, w.tail.String())
}
