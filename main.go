package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage:  \n%s FILE_PATH\n", os.Args[0])
		os.Exit(1)
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	c := newWordFrequencyCalculator(f)
	nodes := c.take(20)

	for _, node := range nodes {
		fmt.Printf("%d %v\n", node.freq, node.wn.w)
	}
}
