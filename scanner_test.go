package main

import (
	"strings"
	"testing"

	"github.com/pilu/miniassert"
)

func TestScanner_scan(t *testing.T) {
	text := "hello world, hi"
	s := newScanner(strings.NewReader(text))

	var words []string

	for w, ok := s.scan(); ok; w, ok = s.scan() {
		words = append(words, w.String())
	}

	expected := []string{
		"hello",
		"world",
		"hi",
	}

	miniassert.Equal(t, expected, words)
}
